﻿using BomberWar.Services.Control;
using BomberWar.Services.Map;
using BomberWar.Services.Render;
using BomberWar.Services.Render.Containers;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;

namespace BomberWar
{
    class Program
    {
        public static uint mapSize = 13;
        public static uint boxSize = 48;
        public static uint maxPixSizeX;
        public static uint maxPixSizeY;
        private static MapService _mapService;

        static void Main(string[] args)
        {
            maxPixSizeX = mapSize * boxSize;
            maxPixSizeY = mapSize * boxSize;

            RenderWindow window = new RenderWindow(new SFML.Window.VideoMode(mapSize*boxSize, mapSize * boxSize), "BomberWar");
            window.SetFramerateLimit(60);
            window.Closed += new EventHandler(OnClose);
            window.SetActive();

            _mapService = new MapService(mapSize, mapSize, boxSize);
            RenderService renderService = new RenderService(window);

            /*
            ControlService controlService = new ControlService(layers.GetLayer(Layer.Player), window);
            SetPlayers(controlService);
            */

            //Shape.Shape.Texture = new Texture("Reasources/Images/Blocks/wall_block_1.png");
            //Texture texture = new Texture("Resources\\Images\\Textures\\Blocks\\wall_block_1.png");
            //Sprite sprite = new Sprite(texture);
            //if (!texture.loadFromFile("image.png", IntRect(0, 0, 32, 32)))
            //{
            //    // error...
            //}
            //IntRect intRect = new IntRect();
            //intRect.Top = 48*2+16;
            //intRect.Left = 0;
            //intRect.Width = 48;
            //intRect.Height = 48;
            //Texture texture = new Texture("Resources\\Images\\Textures\\Packs\\junglePack.png", intRect);
            //Sprite sprite = new Sprite(texture);

            while (window.IsOpen)
            {
                window.DispatchEvents();
                window.Clear();

                renderService.DrawAreaLayer(_mapService.Level, Layer.Ground);
                renderService.DrawAreaLayer(_mapService.Level, Layer.GroundDetails);

                /*
                renderService.DrawLayer(layers.GetLayer(Layer.Ground));
                renderService.DrawLayer(layers.GetLayer(Layer.GroundDetails));
                renderService.DrawLayer(layers.GetLayer(Layer.Block));
                renderService.DrawLayer(layers.GetLayer(Layer.Player));
                */
                //window.Draw(sprite);

                window.Display();
            }

        }
        static void OnClose(object sender, EventArgs e)
        {
            // Close the window when OnClose event is received
            RenderWindow window = (RenderWindow)sender;
            window.Close();
        }

        static void SetPlayers(ControlService controlService)
        {
            //controlService.AddPlayer(PlayerKeySet.PlayerA, Color.Blue, boxSize /2, boxSize /2);
            //controlService.AddPlayer(PlayerKeySet.PlayerB, Color.Yellow, boxSize / 2, boxSize * mapSize - boxSize / 2);
            //controlService.AddPlayer(PlayerKeySet.PlayerC, Color.Red, (int)boxSize * (int)mapSize - (int)boxSize /2, (int)boxSize /2);
            //controlService.AddPlayer(PlayerKeySet.PlayerD, Color.Green, (int)boxSize * (int)mapSize - (int)boxSize / 2, (int)boxSize * (int)mapSize - (int)boxSize / 2);
        }

    }
}
