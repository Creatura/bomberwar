﻿using BomberWar.Services.Render.Containers;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace BomberWar.Services.Control
{
    public enum PlayerKeySet
    {
        PlayerA,
        PlayerB, 
        PlayerC,
        PlayerD
    }

    public class ControlService
    {
        private RenderWindow _window;
        private LayerContainer _playerLayer;
        private Dictionary<PlayerController, Thread> _players;

        public ControlService(LayerContainer palyerLayerContainer, RenderWindow window)
        {
            _window = window;
            _playerLayer = palyerLayerContainer;
            _players = new Dictionary<PlayerController, Thread>();
        }

        /*
        public void AddPlayer(PlayerKeySet playerKeySet, Color color, uint iniX, uint iniY)
        {
            PlayerController player = new PlayerController(_window, KeyboardController.GetKeySet(playerKeySet), iniX, iniY, color);
            Thread playerThread = new Thread(player.Start);

            _players.Add(player, playerThread);
            _playerLayer.Content.Add(player.Shape);

            playerThread.IsBackground = true;
            playerThread.Start();
        }
        */
    }
}
