﻿using BomberWar.Services.Render.Objects;
using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace BomberWar.Services.Control
{
    public class PlayerController
    {
        private KeyboardController _keyboard;
        private float _positionX = 0;
        private float _positionY = 0;
        private uint _size = 36;
        private RenderWindow _window;
        private int _moveStep = 3;

        public BaseGraphicObject Shape;

        public float X {
            get
            {
                return _positionX;
            }
            set
            {
                _positionX = value;
                Shape.PositionX = _positionX;
            }
        }

        public float Y
        {
            get
            {
                return _positionY;
            }
            set
            {
                _positionY = value;
                Shape.PositionY = _positionY;
            }
        }

        public PlayerController(RenderWindow window, KeyboardController keyboard, uint iniX, uint iniY, Color color)
        {
            _window = window;
            _keyboard = keyboard;

            Shape = new BaseGraphicObject(ShapeType.React, color, _size, _size, iniX, iniY);
            Shape.Shape.OutlineThickness = 1;
            Shape.Shape.OutlineColor = Color.Black;

            X = iniX;
            Y = iniY;

            _window.KeyPressed += new EventHandler<KeyEventArgs>(keyboardKeyPressed);
            _window.KeyReleased += new EventHandler<KeyEventArgs>(keyboardKeyReleased);
        }

        public void Start()
        {
            int delay = 20;
            while (true)
            {
                Thread.Sleep(delay);

                if (_keyboard.isMovingUp)
                    MoveUp();
                if (_keyboard.isMovingDown)
                    MoveDown();
                if (_keyboard.isMovingRight)
                    MoveRight();
                if (_keyboard.isMovingLeft)
                    MoveLeft();
                //X++;
            }
        }

        private void keyboardKeyPressed(object sender, KeyEventArgs e)
        {
            _keyboard.SetKey(e, true);
        }

        private void keyboardKeyReleased(object sender, KeyEventArgs e)
        {
            _keyboard.SetKey(e, false);
        }

        private void MoveUp()
        {
            float tmp = Y - _moveStep;
            if (tmp-(_size/2) <= 0)
                return;
            Y = tmp;
        }
        
        private void MoveDown()
        {
            float tmp = Y + _moveStep;
            if (tmp + (_size / 2) >= Program.maxPixSizeY)
                return;
            Y = tmp;
        }

        private void MoveLeft()
        {
            float tmp = X - _moveStep;
            if (tmp - (_size / 2) <= 0)
                return;
            X = tmp;
        }

        private void MoveRight()
        {
            float tmp = X + _moveStep;
            if (tmp + (_size / 2) >= Program.maxPixSizeX)
                return;
            X = tmp;
        }
    }


}
