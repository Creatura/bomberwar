﻿using SFML.Window;
using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Control
{
    public class KeyboardController
    {
        public bool isMovingUp = false;
        public bool isMovingDown = false;
        public bool isMovingLeft = false;
        public bool isMovingRight = false;

        public Keyboard.Key MoveUp;
        public Keyboard.Key MoveDown;
        public Keyboard.Key MoveLeft;
        public Keyboard.Key MoveRight;

        public void SetKey(KeyEventArgs e, bool moveObject)
        {
            if (e.Code == MoveUp)
            {
                isMovingUp = moveObject;
            }
            if (e.Code == MoveDown)
            {
                isMovingDown = moveObject;
            }
            if (e.Code == MoveLeft)
            {
                isMovingLeft = moveObject;
            }
            if (e.Code == MoveRight)
            {
                isMovingRight = moveObject;
            }
        }

        public static KeyboardController GetKeySet(PlayerKeySet keyboardSets)
        {
            KeyboardController playerKeyboardController = new KeyboardController();

            if (keyboardSets == PlayerKeySet.PlayerA)
            {
                playerKeyboardController.MoveUp = Keyboard.Key.Up;
                playerKeyboardController.MoveDown = Keyboard.Key.Down;
                playerKeyboardController.MoveLeft = Keyboard.Key.Left;
                playerKeyboardController.MoveRight = Keyboard.Key.Right;
            }
            else if (keyboardSets == PlayerKeySet.PlayerB)
            {
                playerKeyboardController.MoveUp = Keyboard.Key.W;
                playerKeyboardController.MoveDown = Keyboard.Key.S;
                playerKeyboardController.MoveLeft = Keyboard.Key.A;
                playerKeyboardController.MoveRight = Keyboard.Key.D;
            }
            else if (keyboardSets == PlayerKeySet.PlayerC)
            {
                playerKeyboardController.MoveUp = Keyboard.Key.I;
                playerKeyboardController.MoveDown = Keyboard.Key.K;
                playerKeyboardController.MoveLeft = Keyboard.Key.J;
                playerKeyboardController.MoveRight = Keyboard.Key.L;
            }
            else if (keyboardSets == PlayerKeySet.PlayerD)
            {
                playerKeyboardController.MoveUp = Keyboard.Key.Numpad5;
                playerKeyboardController.MoveDown = Keyboard.Key.Numpad2;
                playerKeyboardController.MoveLeft = Keyboard.Key.Numpad1;
                playerKeyboardController.MoveRight = Keyboard.Key.Numpad3;
            }

            return playerKeyboardController;
        }
    }
}
