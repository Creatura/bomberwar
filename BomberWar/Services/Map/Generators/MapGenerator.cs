﻿using BomberWar.Services.Map.Containers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Map.Generators
{
    enum BlockType
    {
        UndestroyableBlock1 = 1,
        DestroyableBlock1 = 2,
        DestroyableBlock2 = 3,
    }

    public static class MapGenerator
    {
        public static GeneratedLevelMapContainer Generate(uint sizeX, uint sizeY)
        {
            GeneratedLevelMapContainer levelMap = new GeneratedLevelMapContainer(sizeX, sizeY);
            CreateBlocksOnMap(ref levelMap);
            CreateWallsOnMap(ref levelMap);
            ClearStartingPoints(ref levelMap);

            return levelMap;
        }

        public static void CreateBlocksOnMap(ref GeneratedLevelMapContainer level)
        {
            Random rnd = new Random();

            for (int i=0; i<level.SizeX; i++)
            {
                for (int j = 0; j < level.SizeY; j++)
                {
                    var blockType = rnd.Next(0, 16);
                    if (blockType != (int)BlockType.UndestroyableBlock1)
                        level.Map[i, j] = blockType; 
                }
            }
        }

        public static void CreateWallsOnMap(ref GeneratedLevelMapContainer level)
        {
            Random rnd = new Random();

            for (int x = 0; x < level.SizeX; x++)
            {
                for (int y = 0; y < level.SizeY; y++)
                {
                    if((x>0 && y > 0) && (x < level.SizeX-1 && y < level.SizeY-1))
                    {
                        if (x%2==1 && y%2==1)
                        {
                            level.Map[x, y] = (int)BlockType.UndestroyableBlock1;
                        }
                    }
                }
            }
        }

        public static void ClearStartingPoints(ref GeneratedLevelMapContainer level)
        {
            for (int x = 0; x < level.SizeX; x++)
            {
                for (int y = 0; y < level.SizeY; y++)
                {
                    if(x == 0)
                    {
                        if(y==0)
                            level.Map[x, y] = 0;
                        else if (y == 1)
                            level.Map[x, y] = 0;
                        else if (y == level.SizeY - 1)
                            level.Map[x, y] = 0;
                        else if (y == level.SizeY - 2)
                            level.Map[x, y] = 0;
                    }
                    else if (x == 1)
                    {
                        if (y == 0)
                            level.Map[x, y] = 0;
                        else if (y == level.SizeY - 1)
                            level.Map[x, y] = 0;
                    }
                    else if (x == level.SizeX - 1)
                    {
                        if (y == 0)
                            level.Map[x, y] = 0;
                        else if (y == 1)
                            level.Map[x, y] = 0;
                        else if (y == level.SizeY - 1)
                            level.Map[x, y] = 0;
                        else if (y == level.SizeY - 2)
                            level.Map[x, y] = 0;
                    }
                    else if (x == level.SizeX - 2)
                    {
                        if (y == 0)
                            level.Map[x, y] = 0;
                        else if (y == level.SizeY - 1)
                            level.Map[x, y] = 0;
                    }
                }
            }
        }
    }
}
