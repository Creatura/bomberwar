﻿using BomberWar.Services.Map.Containers;
using BomberWar.Services.Map.Generators;
using BomberWar.Services.Render.Containers;
using BomberWar.Services.Render.Objects;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace BomberWar.Services.Map
{
    public class MapService
    {
        private GeneratedLevelMapContainer _currentLevelMap;
        public LevelContainer Level;

        public MapService(uint mapSizeX, uint mapSizeY, uint squareSize)
        {
            _currentLevelMap = MapGenerator.Generate(mapSizeX, mapSizeY);
            Level = new LevelContainer(_currentLevelMap, squareSize);

            //CreateGraphicBlockObjects(layers.GetLayer(Layer.Block));
        }

        /*

        public void CreateGraphicBlockObjects(LayerContainer layerContainer)
        {
            Texture wall = new Texture("Resources\\Images\\Textures\\Blocks\\Block_C_02.png");
            Texture box1 = new Texture("Resources\\Images\\Textures\\Blocks\\Block_A_02.png");

            for (int x = 0; x < _currentLevelMap.SizeX; x++)
            {
                for (int y = 0; y < _currentLevelMap.SizeY; y++)
                {
                    uint positionX = (uint)(x * _squareSize + (_squareSize / 2));
                    uint positionY = (uint)(y * _squareSize + (_squareSize / 2));

                    if (_currentLevelMap.Map[x, y] == 1)
                    {
                        BaseGraphicObject item = new BaseGraphicObject(ShapeType.React, null, _squareSize, _squareSize, positionX, positionY);
                        item.Shape.Texture = wall;
                        layerContainer.Content.Add(item);
                    }
                    else if (_currentLevelMap.Map[x, y] > 1 && _currentLevelMap.Map[x, y] < 16)
                    {
                        BaseGraphicObject item = new BaseGraphicObject(ShapeType.React, null, _squareSize, _squareSize, positionX, positionY);
                        item.Shape.Texture = box1;
                        layerContainer.Content.Add(item);
                    }


                }
            }

        }
        */

    }
}
