﻿using BomberWar.Services.Render.Containers;
using BomberWar.Services.Render.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Map.Containers
{
    public class AreaContainer
    {
        private Layers _layers = new Layers();

        public void Add(Layer layer, BaseGraphicObject item)
        {
            _layers.GetLayer(layer).Add(item);
        }

        public LayerContainer GetLayer(Layer layer)
        {
            return _layers.GetLayer(layer);
        }
    }
}
