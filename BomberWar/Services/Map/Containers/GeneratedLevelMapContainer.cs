﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Map.Containers
{
    public class GeneratedLevelMapContainer
    {
        private int[,] _map;

        public int[,] Map
        {
            get
            {
                if (_map == null)
                {
                    _map = new int[SizeX, SizeY];
                }
                return _map;
            }
            set
            {
                _map = value;
            }
        }

        public uint SizeX { get; set; }
        public uint SizeY { get; set; }

        public GeneratedLevelMapContainer(uint sizeX, uint sizeY)
        {
            SizeX = sizeX;
            SizeY = sizeY;
        }
    }
}
