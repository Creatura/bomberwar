﻿using BomberWar.Services.Render.Containers;
using BomberWar.Services.Render.Objects;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Map.Containers
{
    public class LevelContainer
    {
        public uint mapSizeX;
        public uint mapSizeY;
        private uint _squareSize;

        private AreaContainer[,] _map;

        public LevelContainer(GeneratedLevelMapContainer map, uint squareSize)
        {
            mapSizeX = map.SizeX;
            mapSizeY = map.SizeY;

            _squareSize = squareSize;
            _map        = new AreaContainer[mapSizeX, mapSizeY];

            CreateBackGroud(map);
            CreateBackGroudDetails(map);
        }

        public AreaContainer GetArea(uint x, uint y)
        {
            return _map[x, y];
        }

        private void CreateBackGroud(GeneratedLevelMapContainer map)
        {
            Texture ground1 = new Texture("Resources\\Images\\Textures\\Grounds\\Ground_Tile_01_C.png");
            Texture ground2 = new Texture("Resources\\Images\\Textures\\Grounds\\Ground_Tile_02_C.png");

            for (int x = 0; x < map.SizeX; x++)
            {
                for (int y = 0; y < map.SizeY; y++)
                {
                    uint positionX = (uint)(x * _squareSize + (_squareSize / 2));
                    uint positionY = (uint)(y * _squareSize + (_squareSize / 2));

                    BaseGraphicObject item = new BaseGraphicObject(ShapeType.React, null, _squareSize, _squareSize, positionX, positionY);
                    if ((x % 2 == 0 && y % 2 == 0) || (x % 2 == 1 && y % 2 == 1))
                        item.Shape.Texture = ground1;
                    else
                        item.Shape.Texture = ground2;

                    if (_map[x, y] == null)
                        _map[x, y] = new AreaContainer();

                    _map[x, y].Add(Layer.Ground, item);
                }
            }
        }

        private void CreateBackGroudDetails(GeneratedLevelMapContainer map)
        {
            Texture ground1 = new Texture("Resources\\Images\\Textures\\Others\\Rock_01.png");
            Texture ground2 = new Texture("Resources\\Images\\Textures\\Others\\Rock_03.png");
            Texture ground3a = new Texture("Resources\\Images\\Textures\\Others\\Tree_07.png");
            Texture ground3b = new Texture("Resources\\Images\\Textures\\Others\\Tree_08.png");
            Texture ground3c = new Texture("Resources\\Images\\Textures\\Others\\Tree_09.png");
            Texture ground4 = new Texture("Resources\\Images\\Textures\\Others\\Cactus_01.png");

            Random rnd = new Random();

            for (int x = 0; x < map.SizeX; x++)
            {
                for (int y = 0; y < map.SizeY; y++)
                {
                    uint positionX = (uint)(x * _squareSize + (_squareSize / 2));
                    uint positionY = (uint)(y * _squareSize + (_squareSize / 2));


                    var result1 = rnd.Next(0, 50);
                    var result2 = rnd.Next(0, 50);
                    var result3 = rnd.Next(0, 50);
                    var result4 = rnd.Next(0, 50);

                    if (result1 == 0)
                    {
                        BaseGraphicObject item = new BaseGraphicObject(ShapeType.React, null, _squareSize, _squareSize, positionX, positionY);
                        item.Shape.Texture = ground1;
                        _map[x, y].Add(Layer.GroundDetails, item);
                    }
                    else if (result2 == 0)
                    {
                        BaseGraphicObject item = new BaseGraphicObject(ShapeType.React, null, _squareSize, _squareSize, positionX, positionY);
                        item.Shape.Texture = ground2;
                        _map[x, y].Add(Layer.GroundDetails, item);
                    }
                    else if (result3 == 0)
                    {
                        var r1 = rnd.Next(0, 3);
                        BaseGraphicObject item = new BaseGraphicObject(ShapeType.React, null, _squareSize, _squareSize, positionX, positionY);
                        if(r1==0)
                            item.Shape.Texture = ground3a;
                        if (r1 == 1)
                            item.Shape.Texture = ground3b;
                        if (r1 == 2)
                            item.Shape.Texture = ground3c;
                        _map[x, y].Add(Layer.GroundDetails, item);
                    }
                    else if (result4 == 0)
                    {
                        BaseGraphicObject item = new BaseGraphicObject(ShapeType.React, null, _squareSize, _squareSize, positionX, positionY);
                        item.Shape.Texture = ground4;
                        _map[x, y].Add(Layer.GroundDetails, item);
                    }

                }
            }
        }
    }
}
