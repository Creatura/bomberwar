﻿using BomberWar.Services.Map.Containers;
using BomberWar.Services.Render.Containers;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Render
{
    public class RenderService
    {
        private RenderWindow _window;

        public RenderService(RenderWindow window)
        {
            _window = window;
        }

        public void DrawLayer(LayerContainer layer)
        {

            foreach (var item in layer.GetContent())
            {
                if (item.Sprite != null)
                {
                    _window.Draw(item.Sprite);
                }
                else
                {
                    _window.Draw(item.Shape);
                }
            }
        }

        public void DrawAreaLayer(LevelContainer level, Layer layer)
        {

            for (uint x = 0; x < level.mapSizeX; x++)
            {
                for (uint y = 0; y < level.mapSizeX; y++)
                {
                    DrawLayer(level.GetArea(x, y).GetLayer(layer));
                }
            }
        }
    }
}
