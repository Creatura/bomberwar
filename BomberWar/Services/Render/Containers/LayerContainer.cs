﻿using BomberWar.Services.Render.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Render.Containers
{

    public class LayerContainer
    {
        //Elementy na poszczególnej warstwie, np. gracze na warstwie graczy
        private List<BaseGraphicObject> _content { get; set; } = new List<BaseGraphicObject>();

        public void Add(BaseGraphicObject item)
        {
            _content.Add(item);
        }

        public List<BaseGraphicObject> GetContent()
        {
            return _content;
        }
    }
}
