﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Render.Containers
{
    public enum Layer
    {
        Ground,
        GroundDetails,
        Block,
        Item,
        Player,
        Efect
    }

    class Layers
    {
        private Dictionary<Layer, LayerContainer> _layerStack = new Dictionary<Layer, LayerContainer>();
        //Stos warstw: podłoże, przedmioty, gracze, efekty itp;
        //private LayerContainer[] _layerStack = new LayerContainer[5];

        public Layers()
        {
            _layerStack.Add(Layer.Ground, new LayerContainer());
            _layerStack.Add(Layer.GroundDetails, new LayerContainer());
            _layerStack.Add(Layer.Block, new LayerContainer());
            _layerStack.Add(Layer.Item, new LayerContainer());
            _layerStack.Add(Layer.Player, new LayerContainer()); 
            _layerStack.Add(Layer.Efect, new LayerContainer());
        }

        public LayerContainer GetLayer(Layer layer)
        {
            LayerContainer tmp;
            _layerStack.TryGetValue(layer, out tmp);
            if (tmp == null)
                throw new Exception("Ups");

            return tmp;
        }
    }
}
