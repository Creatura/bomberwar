﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Text;

namespace BomberWar.Services.Render.Objects
{
    public enum ShapeType
    {
        Circle, React
    }

    public class BaseGraphicObject
    {
        public Shape Shape { get; set; }
        public Sprite Sprite { get; set; }

        private float _positionX = 0;
        private float _positionY = 0;

        public float PositionX
        {
            get
            {
                return _positionX;
            }
            set
            {
                _positionX = value;
                Shape.Position = new SFML.System.Vector2f(_positionX - (SizeX / 2), _positionY - (SizeY / 2));
            }
        }

        public float PositionY
        {
            get
            {
                return _positionY;
            }
            set
            {
                _positionY = value;
                Shape.Position = new SFML.System.Vector2f(_positionX - (SizeX / 2), _positionY - (SizeY / 2));
            }
        }

        public uint SizeX { get; set; }

        public uint SizeY { get; set; }

        public BaseGraphicObject(ShapeType shape, Color? color, uint sizeX, uint sizeY, uint initialPositionX = 0, uint initialPositionY = 0)
        {
            this.SizeX = sizeX;
            this.SizeY = sizeY;

            if (shape == ShapeType.Circle)
            {
                SizeY = sizeY;
                Shape = new CircleShape(SizeX/2);
            }
            else if (shape == ShapeType.React)
            {
                var kwadrat = new RectangleShape();
                kwadrat.Size = new Vector2f(sizeX, sizeY);
                Shape = kwadrat;
            }
            else
            {
                throw new Exception("there is no such shape implemented");
            }

            if(color != null)
                Shape.FillColor = (Color)color;

            SetPosition(initialPositionX, initialPositionY);
        }

        public void SetPosition(float newPositionX = 0, float newPositionY = 0)
        {
            this.PositionX = newPositionX;
            this.PositionY = newPositionY;
        }

        public void AddSprite(Sprite sprite)
        {
            Sprite = sprite;
            sprite.Position = Shape.Position;
        }
    }
}
